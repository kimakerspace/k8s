# K8s

Kubernetesconfig für unseren k8s Server.

Wir verwenden Ingress, um den Mr. Beam per Domain (https://mrbeam.ki-maker.space) auffindbar zu machen. Aus Sicherheitsgründen sollte das aber nur aus dem Makerspace heraus gehen. Deshalb erlaubt Ingress auf dieser Domain nur Anfragen, deren IP die unseres Vodafone Anschlusses ist. Wenn sich diese mal wieder ändert, müsste diese aktualisiert werden. Dafür einfach folgende Zeile in ingress.yaml ändern:
```yaml
nginx.ingress.kubernetes.io/whitelist-source-range: 37.24.139.179/32
```

## Certificates expired

Es soll schon vorgekommen sein, das die Zertifikate von dem MicroK8s cluster abgelaufen sind. Das resultiert in folgender Fehlermeldung: 

```
Unable to connect to the server: x509: certificate has expired or is not yet valid: current time 2023-09-26T16:12:03+02:00 is after 2023-07-24T16:58:41Z
```

Zu Lösung muss man die Zertifikate neu generieren, das geht wie folgt:

```bash
sudo microk8s refresh-certs --cert=ca.crt
```

Mehr Details dazu gibt es hier: https://microk8s.io/docs/command-reference#heading--microk8s-refresh-certs