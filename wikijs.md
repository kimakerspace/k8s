# Wikijs
WikiJs auf dem Cluster zu installieren, war ein ganz schöner krampf. Die Postgresql-Datenbank ist standartmäßig nicht mit dem gleichen Passwort konfiguriert wie der Rest. (Warum auch immer?). Auch ist es ab und zu vorgekommen, dass die Datenbank ihr Passwort einfach so geändert hat.

Ich glaube ich habe mittlerweile eine Taktik, wie man WikiJs richtig installieren kann:
```sh
helm repo add requarks https://charts.js.wiki
helm install wikijs --set postgresql.postgresqlPassword="password",auth.postgresPassword="password",auth.password="password" requarks/wiki
```

Wenn das Wiki trotzdem down ist, muss ziemlich sicher das Passwort in der Datenbank geändert werden. Dafür mit kubectl in den Datenbankcontainer rein bashen
```sh
kubectl exec -it pod/wikijs-postgresql-0 bash
```
und dann diesem Guide von Bitnami folgen: https://docs.bitnami.com/virtual-machine/infrastructure/postgresql/administration/change-reset-password/

PS: Dieser Guide ist extra nicht im Wiki, da er da wenig bringt, wenn das Wiki nicht tut.